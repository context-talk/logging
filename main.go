package main

import (
	"context"
	"log"
	"net/http"

	"github.com/google/uuid"
	"go.uber.org/zap"
)

type correlationIDType int

const (
	requestIDKey correlationIDType = iota
)

func main() {
	// Set up logger
	logger, err := zap.NewProduction()
	if err != nil {
		log.Fatal("Failed to set logger")
	}
	defer logger.Sync()

	// Set up net
	api := api{
		logger: *logger,
	}
	http.Handle("/", reqIDMiddleware(api.rootHandler()))
	log.Fatal(http.ListenAndServe(":8000", nil))
}

// ctxLogger will read values from the specified context and add them to the
// logger as keys.
func ctxLogger(ctx context.Context, logger zap.Logger) zap.Logger {
	if ctx == nil {
		return logger
	}

	reqID, ok := ctx.Value(requestIDKey).(string)
	if ok {
		logger = *logger.With(zap.String("reqID", reqID))
	}

	return logger
}

type api struct {
	logger zap.Logger
}

func (a *api) rootHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		logger := ctxLogger(r.Context(), a.logger)
		logger.Info("handling /")
		w.Write([]byte("Hello world"))
	})
}

// reqIDMiddleware is the middleware that will attach a unique request ID to
// each request via the context.
func reqIDMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		reqID, _ := uuid.NewRandom()
		reqCtx := context.WithValue(r.Context(), requestIDKey, reqID.String())
		r = r.WithContext(reqCtx)

		next.ServeHTTP(w, r)
	})
}
