module gitlab.com/context-talk/logging

go 1.13

require (
	github.com/google/uuid v1.1.1
	go.uber.org/zap v1.13.0
)
