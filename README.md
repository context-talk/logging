# Logging

This is an example of how you can use context to store any information about the
request that can be used for enriching the logging data later on.